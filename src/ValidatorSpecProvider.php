<?php
namespace Luomus\InputFilter;


use Triplestore\Service\MetadataService;
use Zend\Validator\Date;

class ValidatorSpecProvider implements ValidatorSpecProviderInterface
{

    protected $metadataService;

    public function __construct(MetadataService $metadataService)
    {
        $this->metadataService = $metadataService;
    }


    public function getDateTimeSpec()
    {
        return [
            'name' => 'Date',
            'options' => [
                'format' => self::DATETIME_FORMAT,
                'messages' => [
                    Date::INVALID_DATE => 'Input \'%value%\' does not appear to be a valid date. Place use format "yyyy-MM-dd\'T\'HH:mm:ssZ" where Z is in format like \'+00:00\'',
                ]
            ]
        ];
    }

    public function getDateSpec()
    {
        return [
            'name' => 'Date',
            'options' => [
                'format' => self::DATE_FORMAT,
                'messages' => [
                    Date::INVALID_DATE => 'Input \'%value%\' does not appear to be a valid date. Place use format "yyyy-MM-dd"',
                ]
            ]
        ];
    }

    public function getStringSpec()
    {
        return [
            'name' => 'Luomus\InputFilter\Validator\Type',
        ];
    }

    public function getIntegerSpec($min = null, $max = null)
    {
        if ($max === null && $min === null) {
            return [
                'name' => 'Luomus\InputFilter\Validator\Type',
                'options' => [
                    'expecting' => 'integer'
                ]
            ];
        }
        $options = [];
        if ($min !== null) {
            $options['min'] = $min;
        }
        if ($max !== null) {
            $options['max'] = $max;
        }
        return [
            'name' => 'Luomus\InputFilter\Validator\Integer',
            'options' => $options
        ];
    }

    public function getDecimalSpec()
    {
        return [
            'name' => 'Luomus\InputFilter\Validator\Decimal'
        ];
    }

    public function getBooleanSpec()
    {
        return [
            'name' => 'Luomus\InputFilter\Validator\Type',
            'options' => [
                'expecting' => 'boolean'
            ]
        ];
    }

    public function getGeometrySpec()
    {
        return [
            'name' => 'Luomus\InputFilter\Validator\Geometry'
        ];
    }

    public function getAltSpec($alt, $field = null)
    {
        return [
            'name' => 'Luomus\InputFilter\Validator\InArray',
            'options' => [
                'haystack' => $this->getAltValues($alt)
            ]
        ];
    }

    public function getObjectExistsSpec($class)
    {
        return [
            'name' => 'Luomus\InputFilter\Validator\ObjectExistsChecker',
            'options' => [
                'type' => $class
            ]
        ];
    }

    public function getAltValues($alt) {
        if (!$this->metadataService->altExists($alt)) {
            return '';
        }
        return  array_keys($this->metadataService->getAlt($alt));
    }

    public function getAnyURISpec()
    {
        return [
            'name' => 'Uri',
            'options' => [
                'allowRelative' => false,
                'allowAbsolute' => true,
            ]
        ];
    }

    public function getKeyAnySpec()
    {
        return [
            'name' => 'Luomus\InputFilter\Validator\KeyValue'
        ];
    }

    public function getKeyValueSpec()
    {
        return [
            'name' => 'Luomus\InputFilter\Validator\KeyValue',
            'options' => [
                'valueType' => 'string'
            ]
        ];
    }

    public function makeArray($spec)
    {
        return [
            'name' => 'Luomus\InputFilter\Validator\ArrayType',
            'options' => [
                'validator' => $spec
            ]
        ];
    }

    public function makeMultiLanguage($spec)
    {
        return [
            'name' => 'Luomus\InputFilter\Validator\MultiLanguage',
            'options' => [
                'validator' => $spec
            ]
        ];
    }

    public function getEmbeddedObjectSpec($className, $property, $hasMany, $isRequired)
    {
        return '';
    }
}