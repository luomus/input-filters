<?php

namespace Luomus\InputFilter;

use Traversable;
use Zend\InputFilter\CollectionInputFilter;
use Zend\InputFilter\InputFilter;
use Zend\InputFilter\Exception;
use Zend\Stdlib\ArrayUtils;


class BaseInputFilter extends InputFilter
{
    protected $collections = [];

    public function setDataWithoutPopulate($data) {
        if ($data instanceof Traversable) {
            $data = ArrayUtils::iteratorToArray($data);
        }
        if (!is_array($data)) {
            throw new Exception\InvalidArgumentException(sprintf(
                '%s expects an array or Traversable argument; received %s',
                __METHOD__,
                (is_object($data) ? get_class($data) : gettype($data))
            ));
        }
        $this->data = $data;
    }

    /**
     * Is the data set has unknown input ?
     *
     * @throws Exception\RuntimeException
     * @return bool
     */
    public function hasUnknown()
    {
        if (null === $this->data) {
            throw new Exception\RuntimeException(sprintf(
                '%s: no data present!',
                __METHOD__
            ));
        }

        $data   = array_keys($this->data);
        $inputs = array_keys($this->inputs);
        $diff   = array_diff($data, $inputs);
        if (!empty($diff)) {
            return count(array_intersect($diff, $inputs)) == 0;
        }

        // Go through all the collections to see if any of them have fields that are missing
        $hasUnknown = false;
        foreach($this->collections as $collection) {
            if (!isset($this->data[$collection])) {
                continue;
            }
            /** @var CollectionInputFilter $collectionFilter */
            $collectionFilter = $this->get($collection);
            $inputFilter = clone $collectionFilter->getInputFilter();
            foreach($this->data[$collection] as $key => $collectionData) {
                if ($inputFilter instanceof BaseInputFilter) {
                    $inputFilter->setDataWithoutPopulate($collectionData);
                } else {
                    $inputFilter->setData($collectionData);
                }
                if ($inputFilter->hasUnknown()) {
                    $hasUnknown = true;
                    break 2;
                }
            }
        }

        return $hasUnknown;
    }

    /**
     * Return the unknown input
     *
     * @throws Exception\RuntimeException
     * @return array
     */
    public function getUnknown()
    {
        if (null === $this->data) {
            throw new Exception\RuntimeException(sprintf(
                '%s: no data present!',
                __METHOD__
            ));
        }

        $data   = array_keys($this->data);
        $inputs = array_keys($this->inputs);
        $diff   = array_diff($data, $inputs);

        $unknownInputs = [];
        $intersect     = array_intersect($diff, $data);
        if (!empty($intersect)) {
            foreach ($intersect as $key) {
                $unknownInputs[$key] = $this->data[$key];
            }
        }

        // Go through all the collections to see if any of them have fields that are missing
        foreach($this->collections as $collection) {
            if (!isset($this->data[$collection])) {
                continue;
            }
            /** @var CollectionInputFilter $collectionFilter */
            $collectionFilter = $this->get($collection);
            $inputFilter = clone $collectionFilter->getInputFilter();
            foreach($this->data[$collection] as $key => $collectionData) {
                if ($inputFilter instanceof BaseInputFilter) {
                    $inputFilter->setDataWithoutPopulate($collectionData);
                } else {
                    $inputFilter->setData($collectionData);
                }
                $unknownCollectionInputs = $inputFilter->getUnknown();
                if (!empty($unknownCollectionInputs)) {
                    $unknownInputs = array_merge($unknownInputs, $unknownCollectionInputs);
                }
            }
        }

        return $unknownInputs;
    }
}