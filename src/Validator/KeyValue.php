<?php

namespace Luomus\InputFilter\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;
use Zend\Validator\ValidatorInterface;

class KeyValue extends AbstractValidator
{
    const NOT_ARRAY       = 'notArray';
    const NOT_VALID_KEY   = 'notValidKey';
    const NOT_VALID_VALUE = 'notValidValue';

    protected $valueType;
    protected $valueValidator;

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::NOT_VALID_VALUE => "Expecting value of type %expecting% but got %value%",
    ];

    /**
     * Additional variables available for validation failure messages
     *
     * @var array
     */
    protected $messageVariables = [
        'expecting' => 'expecting',
    ];

    /**
     * Sets the expected valye type
     *
     * @param string $valueType
     */
    public function setValueType($valueType) {
        if ($this->valueType !== $valueType) {
            if (empty($valueType)) {
                $this->valueValidator = null;
            } else {
                $this->valueValidator = new Type();
                $this->valueValidator->setExpecting($valueType);
            }
        }
        $this->valueType = $valueType;
    }

    /**
     * @return null|int
     */
    public function getValueType()
    {
        return $this->valueType;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        if (!is_array($value)) {
            $this->setValue($value);
            $this->error(self::NOT_ARRAY);

            return false;
        }
        foreach ($value as $key => $childValue) {
            if (!is_string($key)) {
                $this->setValue($key);
                $this->error(self::NOT_VALID_KEY);

                return false;
            }
            if ($this->valueValidator instanceof ValidatorInterface) {
                if (!$this->valueValidator->isValid($childValue)) {
                    $this->abstractOptions['messages'][] = $this->valueValidator->getMessages();

                    return false;
                }
            }
        }

        return true;
    }
}