<?php

namespace Luomus\InputFilter\Validator;

use Zend\Validator\Exception;
use Zend\Validator\InArray as ZendInArray;

class InArray extends ZendInArray
{

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::NOT_IN_ARRAY => 'Value should be %collection%, but got \'%value%\'',
    ];

    /**
     * Additional variables available for validation failure messages
     *
     * @var array
     */
    protected $messageVariables = [
        'collection' => 'collection'
    ];

    protected $collection;

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        if (!parent::isValid($value)) {
            $haystack = $this->haystack;
            $last = array_pop($haystack);
            $this->collection = "'" . implode("', '", $haystack) . "'";
            $this->collection .= " or '$last'";
            $this->setValue($value);
            $this->error(self::NOT_IN_ARRAY);
            return false;
        }
        return true;
    }
}