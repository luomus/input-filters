<?php

namespace Luomus\InputFilter\Validator;

use Zend\Stdlib\ArrayUtils;
use Zend\Validator\Exception;
use Zend\Validator\Explode;

class ArrayType extends Explode
{
    const NOT_ARRAY = 'notArray';

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::NOT_ARRAY => 'Value should be a an array, but got %type%',
    ];

    /**
     * Additional variables available for validation failure messages
     *
     * @var array
     */
    protected $messageVariables = [
        'type' => 'type'
    ];

    protected $type;

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        if (!is_array($value)) {
            $this->type = gettype($value);
            $this->setValue($value);
            $this->error(self::NOT_ARRAY);
            return false;
        }

        return parent::isValid($value);
    }
}