<?php

namespace Luomus\InputFilter\Validator;

use Common\Service\IdService;
use Zend\Filter\Word\CamelCaseToUnderscore;
use Zend\ServiceManager\ServiceLocatorAwareInterface;
use Zend\ServiceManager\ServiceLocatorAwareTrait;
use Zend\ServiceManager\ServiceLocatorInterface;
use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class ObjectExistsChecker extends AbstractValidator implements ServiceLocatorAwareInterface
{
    use ServiceLocatorAwareTrait;

    const ID_REGEX = '/^[a-z0-9\.:_\-#@\/]+$/i';
    const NOT_VALID = 'notValid';
    const NOT_CORRECT_FORMAT = 'notCorrectFormat';

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::NOT_VALID => "Value is not in correct format. Should be a string",
        self::NOT_CORRECT_FORMAT => "Resources '%type%' wasn't in correct format (now '%value%')",
    ];

    /**
     * Additional variables available for validation failure messages
     *
     * @var array
     */
    protected $messageVariables = [
        'type' => 'type'
    ];

    /**
     * @var string Type of the object that the checked value should be
     */
    protected $type;

    protected static $exists;

    /**
     * @return string
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        if (empty($value) || !is_string($value)) {
            $this->setValue($value);
            $this->error(self::NOT_VALID);
            return false;
        }
        $type = $this->getType();
        if ($type === null) {
            throw new Exception\RuntimeException("Type is not specified so cannot validate");
        }
        if (strpos($value, 'http') === 0 || !preg_match(self::ID_REGEX, $value)) {
            $this->type = IdService::getDotLess($type);
            $this->setValue($value);
            $this->error(self::NOT_CORRECT_FORMAT);
            return false;
        }
        return true;
    }
}