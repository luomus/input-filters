<?php

namespace Luomus\InputFilter\Validator;


use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

/**
 * Class Geometry follows the geoJsons geometry object spec
 * @package LajiStore\Validator
 */
class Geometry extends AbstractValidator
{

    const MISSING_TYPE = 'missingType';
    const INVALID_TYPE = 'invalidType';
    const MISSING_GEOMETRIES = 'missingGeometries';
    const INVALID_GEOMETRIES = 'invalidGeometries';
    const MISSING_COORDINATES = 'missingCoordinates';
    const INVALID_COORDINATES = 'invalidCoordinates';
    const INVALID_OBJECT_KEY = 'invalidObjectKey';
    const NOT_SUPPORTED = 'notSupported';

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::MISSING_TYPE => "Geometry is missing type",
        self::INVALID_TYPE => "Invalid type %value% given.",
        self::MISSING_COORDINATES => "Geometry is missing coordinates",
        self::INVALID_COORDINATES => "Coordinates are not in correct format",
        self::INVALID_OBJECT_KEY => "Unknown geometry key '%value%' given",
        self::MISSING_GEOMETRIES => "You need to specify geometries when using type GeometryCollection",
        self::INVALID_COORDINATES => "geometries are only allowed when using type GeometryCollection",
        self::NOT_SUPPORTED => 'Selected geometry type %value% is not yet supported'
    ];

    protected $types = [
        'Point',
        'MultiPoint',
        'LineString',
        'MultiLineString',
        'Polygon',
        'MultiPolygon',
        'GeometryCollection'
    ];

    protected $allowedKeys = [
        'type',
        'coordinates',
        'geometries',
        'coordinateVerbatim',
        'crs'
    ];

    protected $mustHaveCrs = false;

    /**
     * @return boolean
     */
    public function isMustHaveCrs()
    {
        return $this->mustHaveCrs;
    }

    /**
     * @param boolean $mustHaveCrs
     */
    public function setMustHaveCrs($mustHaveCrs)
    {
        $this->mustHaveCrs = $mustHaveCrs;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        return $this->validateGeoJson($value);
    }

    private function validateGeoJson($geoJson) {
        if (!is_array($geoJson) || !isset($geoJson['type'])) {
            $this->error(self::MISSING_TYPE);
            return false;
        }
        if (!is_string($geoJson['type']) || !in_array($geoJson['type'], $this->types)) {
            $this->setValue($geoJson['type']);
            $this->error(self::INVALID_TYPE);
            return false;
        }
        $allowed = $this->allowedKeys;
        // Hack to allow circle
        if ($geoJson['type'] === 'Point') {
            $allowed[] = 'radius';
        }
        $diff = array_diff(array_keys($geoJson), $allowed);
        if (count($diff)) {
            $this->setValue(implode(', ', $diff));
            $this->error(self::INVALID_OBJECT_KEY);
            return false;
        }

        if ($geoJson['type'] == 'GeometryCollection') {
            if (!isset($geoJson['geometries'])) {
                $this->error(self::MISSING_GEOMETRIES);
                return false;
            }
            foreach($geoJson['geometries'] as $geometry) {
                if (!$this->validateGeoJson($geometry)) {
                    return false;
                }
            }
            return true;
        }
        if (!isset($geoJson['coordinates'])) {
            $this->error(self::MISSING_COORDINATES);
            return false;
        }
        if (!$this->checkCoordinates($geoJson['coordinates'], $geoJson['type'])) {
            $this->setValue($geoJson['coordinates']);
            $this->error(self::INVALID_COORDINATES);
            return false;
        }
        if (isset($geoJson['geometries'])) {
            $this->setValue($geoJson['geometries']);
            $this->error(self::INVALID_GEOMETRIES);
            return false;
        }
        return true;
    }

    private function checkCoordinates($coordinates, $type) {
        switch($type) {
            case 'Point':
                return isset($coordinates[0])
                    && isset($coordinates[1])
                    && (is_float($coordinates[0]) || is_int($coordinates[0]))
                    && (is_float($coordinates[1])  || is_int($coordinates[1]))
                    && (!isset($coordinates[2]) || (is_float($coordinates[2]) || is_int($coordinates[2])))
                    && count($coordinates) <= 3;
            case 'LineString':
                return isset($coordinates[0])
                    && isset($coordinates[1])
                    && $this->checkCoordinates($coordinates[0], 'Point')
                    && $this->checkCoordinates($coordinates[1], 'Point');
            case 'Polygon':
                foreach($coordinates as $coordinateSet) {
                    if (!is_array($coordinateSet)) {
                        return false;
                    }
                    foreach($coordinateSet as $coordinate) {
                        if (!$this->checkCoordinates($coordinate, 'Point')) {
                            return false;
                        }
                    }
                }
                return true;
            case 'MultiPoint':
                foreach($coordinates as $coordinate) {
                    if (!$this->checkCoordinates($coordinate, 'Point')) {
                        return false;
                    }
                }
                return true;
            case 'MultiLineString':
                foreach($coordinates as $coordinate) {
                    if (!$this->checkCoordinates($coordinate, 'LineString')) {
                        return false;
                    }
                }
                return true;
            default:
                $this->setValue($type);
                $this->error(self::NOT_SUPPORTED);
                return false;
        }
    }
}