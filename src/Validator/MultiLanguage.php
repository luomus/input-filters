<?php

namespace Luomus\InputFilter\Validator;

use Traversable;
use Zend\Validator\Exception;
use Zend\Validator\Explode;
use Zend\Stdlib\ArrayUtils;

class MultiLanguage extends Explode
{
    const INVALID = 'multiLanguageInvalid';
    const NOT_SUPPORTED_LANGUAGE = 'notSupportedLanguage';

    protected $languages = [
        'en',
        'fi',
        'sv'
    ];

    protected $languagesStr;

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::INVALID => "Invalid language value given. Should be an object with languages as keys.",
        self::NOT_SUPPORTED_LANGUAGE => "Unsupported language key. Should be one of  %languages%."
    ];

    /**
     * Additional variables available for validation failure messages
     *
     * @var array
     */
    protected $messageVariables = [
        'languages' => 'languagesStr',
    ];

    /**
     * @return array
     */
    public function getLanguages()
    {
        return $this->languages;
    }

    /**
     * @param array $languages
     */
    public function setLanguages(array $languages)
    {
        $this->languages = $languages;
    }

    /**
     * Defined by Zend\Validator\ValidatorInterface
     *
     * Returns true if all language values validate true
     *
     * @param  mixed $value
     * @param  mixed $context Extra "context" to provide the composed validator
     * @return bool
     * @throws Exception\RuntimeException
     */
    public function isValid($value, $context = null)
    {
        $this->setValue($value);

        if ($value instanceof Traversable) {
            $value = ArrayUtils::iteratorToArray($value);
        }

        if (is_array($value)) {
            $values = $value;
        } else {
            $this->setValue($value);
            $this->error(self::INVALID);
            return false;
        }

        $validator = $this->getValidator();

        if (!$validator) {
            throw new Exception\RuntimeException(sprintf(
                '%s expects a validator to be set; none given',
                __METHOD__
            ));
        }

        $this->languagesStr = implode(', ', $this->languages);

        foreach ($values as $lang => $value) {
            if (!in_array($lang, $this->languages)) {
                $this->abstractOptions['messages'][$lang] = $this->createMessage(self::NOT_SUPPORTED_LANGUAGE, $value);
                if ($this->isBreakOnFirstFailure()) {
                    return false;
                }
            }
            if (!$validator->isValid($value, $context)) {
                $this->abstractOptions['messages'][] = $validator->getMessages();

                if ($this->isBreakOnFirstFailure()) {
                    return false;
                }
            }
        }

        return count($this->abstractOptions['messages']) == 0;
    }
}