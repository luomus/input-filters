<?php

namespace Luomus\InputFilter\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class Type extends AbstractValidator
{
    const NOT_VALID_TYPE = 'notValidType';

    protected $expecting = 'string';

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::NOT_VALID_TYPE => "Expecting value of type %expecting% but got %value%",
    ];

    /**
     * Additional variables available for validation failure messages
     *
     * @var array
     */
    protected $messageVariables = [
        'expecting' => 'expecting',
    ];

    /**
     * Sets the expected valye type
     *
     * @param string $expecting
     */
    public function setExpecting($expecting) {
        $this->expecting = $expecting;
    }

    /**
     * @return null|int
     */
    public function getExpecting()
    {
        return $this->expecting;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        $currentType = gettype($value);
        if ($currentType !== $this->expecting) {
            $this->setValue($currentType);
            $this->error(self::NOT_VALID_TYPE);
            return false;
        }
        return true;
    }
}