<?php

namespace Luomus\InputFilter\Validator;

use Zend\Validator\AbstractValidator;
use Zend\Validator\Exception;

class Integer extends AbstractValidator
{
    const NOT_INTEGER = 'notInteger';
    const TOO_SMALL = 'tooSmall';
    const TOO_BIG = 'tooBig';

    protected $min;

    protected $max;

    /**
     * @var array
     */
    protected $messageTemplates = [
        self::NOT_INTEGER => "Value '%value%' is not an integer",
        self::TOO_SMALL => "Value should not be smaller than %min% but was %value%",
        self::TOO_BIG => "Value should not be greater than %max% but was %value%"
    ];

    /**
     * Additional variables available for validation failure messages
     *
     * @var array
     */
    protected $messageVariables = [
        'min' => 'min',
        'max' => 'max',
    ];

    /**
     * Sets the minimum value
     *
     * @param int $min
     */
    public function setMin($min) {
        $this->min = $min;
    }

    /**
     * @return null|int
     */
    public function getMin()
    {
        return $this->min;
    }

    /**
     * Sets the maximum value
     *
     * @param int $max
     */
    public function setMax($max) {
        $this->max = $max;
    }

    /**
     * @return null|int
     */
    public function getMax()
    {
        return $this->max;
    }

    /**
     * Returns true if and only if $value meets the validation requirements
     *
     * If $value fails validation, then this method returns false, and
     * getMessages() will return an array of messages that explain why the
     * validation failed.
     *
     * @param  mixed $value
     * @return bool
     * @throws Exception\RuntimeException If validation of $value is impossible
     */
    public function isValid($value)
    {
        $this->setValue($value);
        if (!is_integer($value)) {
            $this->error(self::NOT_INTEGER);
            return false;
        }
        if ($this->min !== null && $value < $this->min) {
            $this->error(self::TOO_SMALL);
            return false;
        }
        if ($this->max !== null && $value > $this->max) {
            $this->error(self::TOO_BIG);
            return false;
        }
        return true;
    }
}