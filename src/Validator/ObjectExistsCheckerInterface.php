<?php

namespace Luomus\InputFilter\Validator;


interface ObjectExistsCheckerInterface
{
    public function exists($value, $type);
}