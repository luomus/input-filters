<?php
namespace Luomus\InputFilter;


interface ValidatorSpecProviderInterface
{
    const DATETIME_FORMAT = 'Y-m-d\TH:i:sP';
    const DATE_FORMAT = 'Y-m-d';

    public function getDateTimeSpec();
    public function getDateSpec();
    public function getIntegerSpec($min = null, $max = null);
    public function getDecimalSpec();
    public function getBooleanSpec();
    public function getGeometrySpec();
    public function getStringSpec();
    public function getAltSpec($alt);
    public function getObjectExistsSpec($class);
    public function getAnyURISpec();
    public function getKeyAnySpec();
    public function getKeyValueSpec();
    public function getEmbeddedObjectSpec($className, $property, $hasMany, $isRequired);

    public function makeArray($spec);
    public function makeMultiLanguage($spec);
}