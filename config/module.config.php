<?php
use Luomus\InputFilter\Generator;

return [
    'luomus' => [
        'input_filter' => [
            'namespace' => 'Luomus/V1/GeneratedInputFilter',
            'default_extend' => 'Luomus\Validator\BaseInputFilter',
            'extend' => [
                //'class name' => 'class to be extended'
            ],
            'style' => Generator::STYLE_SHORT,
            'location' => './data/generated-inputfilter'
        ],
    ]
];
